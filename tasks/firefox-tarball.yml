---
- name: "firefox: remove firefox apt package"
  ansible.builtin.apt:
    pkg:
      - firefox
    state: absent

- name: "firefox: get list of snaps"
  ansible.builtin.command: snap list
  register: snaps
  ignore_errors: true
  changed_when: false

- name: "firefox: remove firefox snap"
  ansible.builtin.command: snap remove firefox
  when: "'firefox' in item"
  with_items: "{{ snaps.stdout_lines }}"

- name: "firefox: get available firefox versions"
  local_action:
    module: ansible.builtin.uri
    url: "{{ firefox_versions_url }}"
    return_content: true
  register: firefox_versions

- name: "firefox: set facts about available firefox versions"
  ansible.builtin.set_fact:
    firefox_latest_version: "{{ firefox_versions.json.LATEST_FIREFOX_VERSION }}"

- name: "firefox: check if firefox is installed"
  ansible.builtin.command: firefox --version
  register: firefox_installed_version
  become: false
  ignore_errors: true
  changed_when: false

- name: "firefox: set facts about installed firefox"
  ansible.builtin.set_fact:
    firefox_latest_already_installed: "{{ firefox_latest_version in firefox_installed_version.stdout }}"

- name: "firefox: download & install latest firefox"
  ansible.builtin.unarchive:
    src: https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US
    dest: /opt
    remote_src: true
  when: not firefox_latest_already_installed

- name: "firefox: create local bin directory"
  ansible.builtin.file:
    path: /usr/local/bin
    state: directory
    mode: "0755"

- name: "firefox: symlink firefox to /usr/local/bin"
  ansible.builtin.file:
    src: /opt/firefox/firefox
    dest: /usr/local/bin/firefox
    owner: root
    group: root
    state: link

- name: "firefox: create local applications directory"
  ansible.builtin.file:
    path: /usr/local/share/applications
    state: directory
    mode: "0755"

- name: "firefox: install desktop file to local applications"
  ansible.builtin.copy:
    src: files/firefox.desktop
    dest: /usr/local/share/applications/firefox.desktop
    force: false  # don't clobber an existing desktop file

- name: "firefox: install unprivileged user namespaces apparmor profile"
  ansible.builtin.copy:
    src: files/firefox-tarball.apparmor
    dest: /etc/apparmor.d/firefox-tarball
    force: true
    mode: "0600"
    owner: root
    group: root
